﻿// C++5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Vector
{
public:
    Vector() : x(1), y(2), z(3)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }

    double Show1()
    {
        int a = x * x + y * y + z * z;

        return sqrt(a);
    }
    
private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector a;
    a.Show();
    
    std::cout << a.Show1();
}
